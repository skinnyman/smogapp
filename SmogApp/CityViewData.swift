import Foundation

struct CityViewData {
    var name: String
    var latitude: Double
    var longitude: Double
    
    init() {
        self.name = "SmogApp"
        self.latitude = 0
        self.longitude = 0
    }
}
