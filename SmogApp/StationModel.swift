import Foundation

struct StationModel {
    let name: String
    let conditionsIndex: Int
    let measurementDate: Int
    var pollutions: [PollutionModel]
}
