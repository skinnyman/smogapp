import UIKit

class PollutionCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var pollutionNameLabel: InfoTextValueLabel!
    @IBOutlet weak var pollutionPercentageValueLabel: UILabel!
    
    var pollution: PollutionsViewData! = nil
    
    func configureCollectionViewCell() {
        pollutionNameLabel.text = pollution.particleName
        pollutionNameLabel.font = UIFont.systemFont(ofSize: 18)
        pollutionPercentageValueLabel.text = pollution.percentageValue
        
        self.layer.borderWidth = 1
        self.layer.borderColor = MainStyles.accent.cgColor
        
        if (isSelected) {
            self.backgroundColor = MainStyles.accent
            pollutionNameLabel.textColor = MainStyles.blank
            pollutionPercentageValueLabel.textColor = MainStyles.blank
        } else {
            self.backgroundColor = MainStyles.blank
            pollutionNameLabel.textColor = MainStyles.primaryDark
            pollutionPercentageValueLabel.textColor = StyleUtils.adjustConditionColor(percentagePollutionContent: pollution.valueNumeric)
        }
    }    
    override func prepareForReuse() {
        pollution = nil
        isSelected = false
    }
}
