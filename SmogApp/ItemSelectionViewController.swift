import UIKit

protocol ItemSelectionDelegate {
    func citySelected(item: ListItem)
    func stationSelected(item: ListItem)
}

class ItemSelectionViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var itemsList = [ListItem]()
    var itemType = ListItemType.city
    var delegate: ItemSelectionDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
        
        switch itemType {
        case .city:
            tableView.isScrollEnabled = true
        case .station:
            tableView.isScrollEnabled = false
        }
    }
}

extension ItemSelectionViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "UserCell")
        
        cell.textLabel?.text = itemsList[indexPath.row].name
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch(itemType) {
        case .city:
            self.delegate?.citySelected(item: itemsList[indexPath.row])
            _ = self.navigationController?.popViewController(animated: true)
        case .station:
            let item = itemsList[indexPath.row]
            
            self.delegate?.stationSelected(item: item)
            dismiss(animated: false, completion: nil)
        }        
    }
}
