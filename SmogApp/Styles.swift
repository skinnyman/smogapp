import Foundation
import UIKit

struct StyleUtils {
    static func adjustConditionColor(percentagePollutionContent: Float) -> UIColor {
        switch(percentagePollutionContent) {
        case 0...0.2:
            return ConditionsColors.veryGood
        case 0.2...0.4:
            return ConditionsColors.good
        case 0.4...0.6:
            return ConditionsColors.average
        case 0.6...0.8:
            return ConditionsColors.bad
        case 0.8...1.0:
            return ConditionsColors.veryBad
        default:
            return ConditionsColors.background
        }
    }
}

struct ConditionsColors {
    static var veryBad = UIColor.init(red: 192/255, green: 57/255, blue: 43/255, alpha: 1)
    static var bad = UIColor.init(red: 230/255, green: 126/255, blue: 34/255, alpha: 1)
    static var average = UIColor.init(red: 241/255, green: 196/255, blue: 15/255, alpha: 1)
    static var good = UIColor.init(red: 39/255, green: 174/255, blue: 96/255, alpha: 1)
    static var veryGood = UIColor.init(red: 46/255, green: 204/255, blue: 113/255, alpha: 1)
    static var background = UIColor.init(red: 236/255, green: 240/255, blue: 241/255, alpha: 1)
}

struct MainStyles {
    static var primary = UIColor.init(red: 189/255, green: 195/255, blue: 199/255, alpha: 1)
    static var primaryDark = UIColor.init(red: 127/255, green: 140/255, blue: 141/255, alpha: 1)
    static var accent = UIColor.init(red: 52/255, green: 152/255, blue: 219/255, alpha: 1)
    static var danger = UIColor.init(red: 192/255, green: 57/255, blue: 43/255, alpha: 1)
    static var blank = UIColor.white
}
