import UIKit

class InfoTextLabel : UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.initActions()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initActions()
    }
    
    func initActions() {
        let co = MainStyles.primary
        self.textColor = co
    }
}

class InfoTextValueLabel : UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.initActions()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initActions()
    }
    
    func initActions() {
        self.textColor = MainStyles.primaryDark
    }
}

class WarningLabel : UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.initActions()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initActions()
    }
    
    func initActions() {
        self.textColor = MainStyles.danger
    }
}

class BigLabel : UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.initActions()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initActions()
    }
    
    func initActions() {
        self.textColor = MainStyles.primary
        self.font = self.font.withSize(40)
    }
}
