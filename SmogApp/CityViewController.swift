import UIKit
import SwiftyJSON
import MagicPie

class CityViewController: UIViewController {
    // MARK: Outlets
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var noNetworkConnectionLabel: WarningLabel!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pieChartContainer: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var stationChangeButton: UIButton!
    
    @IBOutlet weak var pollutionPrecentageValueLabel: BigLabel!
    @IBOutlet weak var pollutionNameLabel: InfoTextLabel!
    @IBOutlet weak var pollutionParticleNameLabel: BigLabel!
    
    @IBOutlet weak var valueLabelsStackView: UIStackView!
    @IBOutlet weak var pollutionValueLabel: InfoTextValueLabel!
    @IBOutlet weak var pollutionMaxValueLabel: InfoTextValueLabel!
    @IBOutlet weak var giosIndexValueLabel: InfoTextValueLabel!
    @IBOutlet weak var measurementTimeValueLabel: InfoTextValueLabel!
    
    // MARK: Properties
    fileprivate let cityPresenter = CityPresenter(cityService: CityService())
    fileprivate var cityToDisplay = CityViewData()
    fileprivate var stationsToDisplay = [StationsViewData]()
    
    let pieLayer = PieLayer()
    var selectedStationIndex = 0
    var selectedPollutionIndex = 0
    var isDataLoaded = false
    
    // MARK: - UIViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.hidesWhenStopped = true
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "PollutionCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PollutionCollectionViewCell")
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 150
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.contentInset = UIEdgeInsets(top: self.headerView.bounds.size.height-64, left: 0, bottom: 0, right: 0)
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: "DescriptionTableViewCell", bundle:nil), forCellReuseIdentifier: "DescriptionTableViewCell")
        
        setUpStyles()
        setupPieChart()
        
        cityPresenter.attachView(self)
        cityPresenter.getData(cityId: nil)
    }
    // MARK: UI Setup
    func setupPieChart() {
        pieLayer.frame = self.pieChartContainer.bounds
        let maxRadius = Float(self.pieChartContainer.bounds.size.height/2)
        pieLayer.maxRadius = maxRadius
        pieLayer.minRadius = maxRadius - 5
        pieLayer.startAngle = 90
        pieLayer.endAngle = -270
        
        pieChartContainer.layer.addSublayer(pieLayer)
        pieLayer.addValues([PieElement(value: 0.0, color: ConditionsColors.veryGood),
                            PieElement(value: 1.0, color: ConditionsColors.background)],
                            animated: true)
    }
    func setUpStyles() {
        self.stationChangeButton.setTitleColor(MainStyles.accent, for: .normal)
    }
    // MARK: UI Updates
    func updateUi() {
        let selectedStation = stationsToDisplay[selectedStationIndex]
        self.stationChangeButton.setTitle(selectedStation.name, for: .normal)
        
        if selectedStation.pollutions.count > 0 {
            let selectedPollution = selectedStation.pollutions[selectedPollutionIndex]
            updatePieChart(newValue: selectedPollution.valueNumeric)
            updatePieChartDetails(pollution: selectedPollution)
            updateHeaderViewDetails(pollution: selectedPollution)
            collectionView.isHidden = false
            tableView.isHidden = false
            tableView.reloadData()
            let indexPath1 = IndexPath(row: 0, section: 0)
            self.tableView.reloadRows(at: [indexPath1], with: UITableViewRowAnimation.right)
            collectionView.selectItem(at: IndexPath(item: selectedPollutionIndex, section: 0), animated: true, scrollPosition: .centeredHorizontally)
        } else {
            updatePieChart(newValue: 0.0)
            updatePieChartDetails(pollution: nil)
            updateHeaderViewDetails(pollution: nil)
            collectionView.isHidden = true
            tableView.isHidden = true
        }
    }
    func updatePieChart(newValue: Float) {
        let pollutionItem = pieLayer.values[0] as! PieElement
        let freeOfPollutionItem = pieLayer.values[1] as! PieElement
        PieElement.animateChanges {
            pollutionItem.val = newValue
            pollutionItem.color = StyleUtils.adjustConditionColor(percentagePollutionContent: newValue)
            freeOfPollutionItem.val = (1-newValue)
        }
    }
    func updatePieChartDetails(pollution: PollutionsViewData?) {        
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState, animations: {
            self.pollutionPrecentageValueLabel.transform = CGAffineTransform(scaleX: 1, y: 0.1)
            self.pollutionNameLabel.transform = CGAffineTransform(scaleX: 1, y: 0.1)
            self.pollutionParticleNameLabel.transform = CGAffineTransform(scaleX: 1, y: 0.1)
            self.pollutionPrecentageValueLabel.alpha = 0.0
            self.pollutionNameLabel.alpha = 0.0
            self.pollutionParticleNameLabel.alpha = 0.0
        }, completion: { (finished: Bool) in
            self.pollutionPrecentageValueLabel.text = pollution?.percentageValue ?? ""
            self.pollutionPrecentageValueLabel.textColor = StyleUtils.adjustConditionColor(percentagePollutionContent: pollution?.valueNumeric ?? 0.0)
            self.pollutionNameLabel.text = pollution?.particleDescription ?? "Brak danych"
            self.pollutionParticleNameLabel.text = pollution?.particleName ?? ""
            
            UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState, animations: {
                self.pollutionPrecentageValueLabel.transform = CGAffineTransform(scaleX: 1, y: 1)
                self.pollutionNameLabel.transform = CGAffineTransform(scaleX: 1, y: 1)
                self.pollutionParticleNameLabel.transform = CGAffineTransform(scaleX: 1, y: 1)
                self.pollutionPrecentageValueLabel.alpha = 1.0
                self.pollutionNameLabel.alpha = 1.0
                self.pollutionParticleNameLabel.alpha = 1.0
            }, completion: nil)
        })
    }
    func updateHeaderViewDetails(pollution: PollutionsViewData?) {
        
        self.valueLabelsStackView.fadeOut(completion: {
            (finished: Bool) -> Void in
            self.pollutionValueLabel.text = pollution?.value ?? "-"
            self.pollutionMaxValueLabel.text = pollution?.maxValue ?? "-"
            self.giosIndexValueLabel.text = pollution?.giosIndex ?? "-"
            self.measurementTimeValueLabel.text = pollution?.measurementTime ?? "-"
            self.valueLabelsStackView.fadeIn()
        })
        
    }
}
// MARK: - UIPopoverPresentationControllerDelegate
extension CityViewController: UIPopoverPresentationControllerDelegate {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowCitiesListSegue" {
            if let vc = segue.destination as? ItemSelectionViewController {
                vc.itemsList = CitiesList.list
                vc.itemType = ListItemType.city
                vc.delegate = self
            }
        }
        else if segue.identifier == "ShowStationsListSegue" {
            if let vc = segue.destination as? ItemSelectionViewController {
                var stationsList = [ListItem]()
                for (index, station) in stationsToDisplay.enumerated() {
                    stationsList.append(ListItem(name: station.name, id: index))
                }
                vc.itemsList = stationsList
                vc.itemType = ListItemType.station
                vc.delegate = self
                
                vc.preferredContentSize = CGSize(width: 300, height: stationsList.count*44)
                let popoverController = vc.popoverPresentationController
                if popoverController != nil {
                    popoverController!.delegate = self
                    popoverController!.sourceRect = stationChangeButton.bounds
                    popoverController!.permittedArrowDirections = UIPopoverArrowDirection(rawValue: UIPopoverArrowDirection.up.rawValue)
                }
            }
        }
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}
// MARK: - ItemSelectionDelegate
extension CityViewController: ItemSelectionDelegate {
    func citySelected(item: ListItem) {
        selectedStationIndex = 0
        selectedPollutionIndex = 0
        cityPresenter.getData(cityId: item.id)
    }
    func stationSelected(item: ListItem) {
        selectedStationIndex = item.id
        selectedPollutionIndex = 0
        stationChangeButton.setTitle(item.name, for: .normal)
        
        collectionView.reloadData()
        tableView.reloadData()
        updateUi()
    }
}
// MARK: - UITableViewDataSource
extension CityViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if isDataLoaded {
            return 1
        } else {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isDataLoaded {
            return 1
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let pollution = self.stationsToDisplay[selectedStationIndex].pollutions[selectedPollutionIndex]
        let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionTableViewCell", for: indexPath) as! DescriptionTableViewCell
        cell.textView.text = PollutionDescription.get(forId: pollution.particleId)
        cell.configureCell()
        cell.textView.delegate = self
        return cell
    }
}
// MARK: - UITableViewDelegate
extension CityViewController: UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.transformHeaderView(offset: self.tableView.contentOffset.y + headerView.bounds.height)
    }
    func transformHeaderView(offset: CGFloat) {
        
        var headerTransform = CATransform3DIdentity
        if offset < 0 {
            let headerScaleFactor:CGFloat = -(offset) / self.headerView.bounds.height
            let headerSizevariation = ((self.headerView.bounds.height * (1.0 + headerScaleFactor)) - self.headerView.bounds.height)/2
            headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
            headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
        } else {
            headerTransform = CATransform3DTranslate(headerTransform, 0, -offset, 0)
        }
        self.headerView.layer.transform = headerTransform
    }
}
// MARK: - UITextViewDelegate
extension CityViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
    }
}
// MARK: - UICollectionViewDataSource
extension CityViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if self.isDataLoaded {
            return 1
        } else {
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.isDataLoaded {
            return self.stationsToDisplay[selectedStationIndex].pollutions.count
        } else {
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "PollutionCollectionViewCell", for: indexPath) as! PollutionCollectionViewCell
        let pollution = self.stationsToDisplay[selectedStationIndex].pollutions[indexPath.row]
        
        cell.pollution = pollution
        cell.configureCollectionViewCell()
        return cell
    }
}
// MARK: - UICollectionViewDelegate
extension CityViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? PollutionCollectionViewCell
        if let cell = cell {
            cell.configureCollectionViewCell()
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? PollutionCollectionViewCell
        if let cell = cell {
            cell.configureCollectionViewCell()
            self.selectedPollutionIndex = indexPath.row
            self.updateUi()
        }
    }
}
// MARK: - CityView
extension CityViewController: CityView {
    func startLoading() {
        isDataLoaded = false
        
        UIView.animate(withDuration: 0.5, delay: 0.3, options: [.transitionCrossDissolve], animations: {
            self.loadingView.alpha = 1.0
            self.loadingView.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: self.loadingView.bounds.size)
        }, completion: nil)
        
        
        activityIndicator.startAnimating()
    }
    func finishLoading() {
        activityIndicator.stopAnimating()
        UIView.animate(withDuration: 0.1, delay: 0.0, options: [.transitionCrossDissolve], animations: {
            self.loadingView.alpha = 0.0
        }, completion: nil)
    }
    func setCityData(_ city: CityViewData) {
        cityToDisplay = city
        self.title = cityToDisplay.name
    }
    func setStationsData(_ stations: [StationsViewData]) {
        self.infoView.isHidden = true
        stationsToDisplay = stations
        isDataLoaded = true
        collectionView.reloadData()
        tableView.reloadData()
        
//        let indexPath1 = IndexPath(row: 0, section: 0)
//        self.tableView.reloadRows(at: [indexPath1], with: UITableViewRowAnimation.fade)
        
        updateUi()
    }
    func setEmptyCity() {
        self.title = "SmogApp"
    }
    func setEmptyStations() {
        self.infoView.isHidden = false
        if currentReachabilityStatus == .notReachable {
            self.noNetworkConnectionLabel.isHidden = false
        } else {
            self.noNetworkConnectionLabel.isHidden = true
        }
    }
}
