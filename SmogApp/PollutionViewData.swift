import Foundation

struct PollutionsViewData {
    var particleName: String
    var particleDescription: String
    var percentageValue: String
    var value: String
    var valueNumeric: Float
    var maxValue: String
    var giosIndex: String
    var measurementTime: String
    var particleId: Int
    
    init() {
        self.particleName = ""
        self.particleDescription = ""
        self.percentageValue = ""
        self.value = "-"
        self.valueNumeric = 0.0
        self.maxValue = "-"
        self.giosIndex = "-"
        self.measurementTime = "-"
        self.particleId = 0
    }
}
