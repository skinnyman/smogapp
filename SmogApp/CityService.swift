import Foundation
import SwiftyJSON

class CityService {
    func getCityDetails(cityId: Int?, _ callBack:@escaping (CityModel?) -> Void) {
        var cityIdString = "01"
        if let cityId = cityId {
            cityIdString = String(format: "%02d", cityId)
            UserDefaults.standard.set(cityIdString, forKey: "lastSelectedCity")
            UserDefaults.standard.synchronize()
        } else {
            if let lastSelectedCity = UserDefaults.standard.value(forKey: "lastSelectedCity") as? String {
                cityIdString = lastSelectedCity
            }
        }
        
        let cityEndpoint: String = "http://powietrze.malopolska.pl/_powietrzeapi/api/dane?act=danemiasta&ci_id=" + cityIdString
        guard let url = URL(string: cityEndpoint) else {
            print("Error: cannot create URL")
            callBack(nil)
            return
        }
        let urlRequest = URLRequest(url: url)
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let getCityDetailsTask = session.dataTask(with: urlRequest) {(data, response, error) in
            guard error == nil else {
                print("Error: \(error)")
                DispatchQueue.main.async {
                    callBack(nil)
                }
                return
            }
            guard let responseData = data else {
                print("Error: did not receive data")
                DispatchQueue.main.async {
                    callBack(nil)
                }
                return
            }
            do {
                let cityModel = self.parseCityData(data: responseData)
                
                DispatchQueue.main.async {
                    callBack(cityModel)
                }
            }
        }
        getCityDetailsTask.resume()
    }
    
    func parseCityData(data: Data) -> CityModel {
        let json = JSON(data: data)
        
        let city = json["dane"]["city"]
        let cityName = city["ci_citydesc"].stringValue
        let latitude = city["ci_lat"].doubleValue
        let longitude = city["ci_lon"].doubleValue
        
        var stations = [StationModel]()
        
        if let cityStations = json["dane"]["actual"].array {
            stations = self.parseStationsData(stations: cityStations)
        }
        
        return CityModel(name: cityName, latitude: latitude, longitude: longitude, stations: stations)
    }
    
    func parseStationsData(stations: [JSON]) -> [StationModel] {
        var parsedStations = [StationModel]()
        
        for station in stations {
            let name = station["station_name"].stringValue
            let conditionsIndex = station["station_max"].intValue
            let measurmentDate = station["station_hour"].intValue
            
            var pollutions = [PollutionModel]()

            if let stationMeasurements = station["details"].array {
                pollutions = self.parsePollutionsData(measurements: stationMeasurements)
            }
            parsedStations.append(StationModel(name: name, conditionsIndex: conditionsIndex, measurementDate: measurmentDate, pollutions: pollutions))
        }
        return parsedStations
    }    
    
    func parsePollutionsData(measurements: [JSON]) -> [PollutionModel] {
        var parsedPollutions = [PollutionModel]()
        
        for measurement in measurements {
            let particleDescription = measurement["par_desc"].stringValue
            let particleUnit = measurement["par_unit"].stringValue
            let particleName = measurement["par_name"].stringValue
            let particleId = Int(measurement["par_id"].stringValue)
            
            let value = Int(measurement["o_value"].stringValue)
            let maxValue = Int(measurement["g_max_val"].stringValue)
            
            let giosIndexName = measurement["g_nazwa"].stringValue
            let measurementTime = measurement["o_czas"].stringValue
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH-mm-ss"
            let measurementDate = dateFormatter.date(from: measurementTime)
            
            parsedPollutions.append(PollutionModel(particleName: particleName, particleDescription: particleDescription, particleUnit: particleUnit, value: value, maxValue: maxValue, giosIndexName: giosIndexName, measurementTime: measurementDate, particleId: particleId))
        }
        return parsedPollutions
    }
}
