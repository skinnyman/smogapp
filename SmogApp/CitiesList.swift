import Foundation

struct CitiesList {
    static var list = [ListItem(name: "Kraków", id: 1),
                ListItem(name: "Tarnów", id: 2),
                ListItem(name: "Nowy Sącz", id: 3),
                ListItem(name: "Bochnia", id: 4),
                ListItem(name: "Brzesko", id: 5),
                ListItem(name: "Chrzanów", id: 6),
                ListItem(name: "Dąbrowa Tarnowska", id: 7),
                ListItem(name: "Gorlice", id: 8),
                ListItem(name: "Krynica Zdrój", id: 9),
                ListItem(name: "Limanowa", id: 10),
                ListItem(name: "Miechów", id: 11),
                ListItem(name: "Myślenice", id: 12),
                ListItem(name: "Nowy Targ", id: 13),
                ListItem(name: "Olkusz", id: 14),
                ListItem(name: "Oświęcim", id: 15),
                ListItem(name: "Proszowice", id: 16),
                ListItem(name: "Skawina", id: 17),
                ListItem(name: "Sucha Beskidzka", id: 18),
                ListItem(name: "Trzebinia", id: 19),
                ListItem(name: "Tuchów", id: 20),
                ListItem(name: "Wadowice", id: 21),
                ListItem(name: "Wieliczka", id: 22),
                ListItem(name: "Zakopane", id: 23)]
}

enum ListItemType {
    case city
    case station
}

struct ListItem {
    var name: String
    var id: Int
}
