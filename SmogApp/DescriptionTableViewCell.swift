import UIKit

class DescriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var textView: UITextView!
    
    func configureCell() {
        textView.textColor = MainStyles.primaryDark
        textView.font = UIFont.systemFont(ofSize: 18)
    }
}
