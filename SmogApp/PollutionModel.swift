import Foundation

struct PollutionModel {
    let particleName: String
    let particleDescription: String
    let particleUnit: String
    let value: Int?
    let maxValue: Int?
    let giosIndexName: String
    let measurementTime: Date?
    let particleId: Int?
}
