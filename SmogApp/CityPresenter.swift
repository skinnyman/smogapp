import Foundation

protocol CityView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func setCityData(_ city: CityViewData)
    func setStationsData(_ stations: [StationsViewData])
    func setEmptyCity()
    func setEmptyStations()
}

class CityPresenter {
    fileprivate let cityService:CityService
    weak fileprivate var cityView : CityView?
    
    init(cityService:CityService){
        self.cityService = cityService
    }
    func attachView(_ view:CityView){
        cityView = view
    }    
    func detachView() {
        cityView = nil
    }
    
    func getData(cityId: Int?){
        self.cityView?.startLoading()
        
        cityService.getCityDetails(cityId: cityId, {[weak self] city in
            guard let city = city else {
                self?.cityView?.finishLoading()
                self?.cityView?.setEmptyCity()
                self?.cityView?.setEmptyStations()
                return
            }
            var mappedCity = CityViewData()
            mappedCity.name = city.name.characters.count > 0 ? city.name : "SmogApp"
            mappedCity.latitude = city.latitude
            mappedCity.longitude = city.longitude
            
            var mappedStations = [StationsViewData]()
            
            for station in city.stations {
                var preparedPollutions = [PollutionsViewData]()
                
                for pollution in station.pollutions {
                    var preparedPollution = PollutionsViewData()
                    
                    preparedPollution.particleDescription = pollution.particleDescription
                    preparedPollution.giosIndex = pollution.giosIndexName.lowercased()
                    preparedPollution.particleName = pollution.particleName
                    
                    if let particleId = pollution.particleId {
                        preparedPollution.particleId = particleId
                    }
                    
                    if let pollutionValue = pollution.value, let pollutionMaxValue = pollution.maxValue {
                        preparedPollution.value = "\(String(pollutionValue)) \(pollution.particleUnit)"
                        preparedPollution.maxValue = "\(String(pollutionMaxValue)) \(pollution.particleUnit)"
                        
                        let percentageValue = Float(pollutionValue)/Float(pollutionMaxValue)
                        preparedPollution.percentageValue = String(format: "%.f%%", percentageValue*100)
                        
                        preparedPollution.valueNumeric = percentageValue > 1.0 ? 1.0 : percentageValue
                    }
                    if let measurementTime = pollution.measurementTime {
                        let dateComponentsFormatter = DateComponentsFormatter()
                        dateComponentsFormatter.allowedUnits = [.day,.hour,.minute]
                        dateComponentsFormatter.maximumUnitCount = 1
                        dateComponentsFormatter.unitsStyle = .full
                        if let measurementDelay = dateComponentsFormatter.string(from: measurementTime, to: Date()) {
                            preparedPollution.measurementTime = measurementDelay
                        }
                    }
                    preparedPollutions.append(preparedPollution)
                }
                mappedStations.append(StationsViewData(name: station.name, pollutions: preparedPollutions))
            }
            self?.cityView?.finishLoading()
            
            if mappedCity.name.characters.count != 0 {
                self?.cityView?.setCityData(mappedCity)
            } else {
                self?.cityView?.setEmptyCity()
            }
            
            if mappedStations.count > 0 {
                self?.cityView?.setStationsData(mappedStations)
            } else {
                self?.cityView?.setEmptyStations()
            }            
        })
    }
}
