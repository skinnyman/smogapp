import Foundation

struct PollutionsDescriptionsStrings {
    static var pm10 = "Pył zawieszony PM10 jest frakcją pyłu o bardzo małych rozmiarach średnicy ziaren - do 10 mikrometrów. Ziarna są wystarczająco małe, aby mogły przeniknąć głęboko do płuc.\n\nGłównym źródłem pyłu PM10 w powietrzu w europejskich miastach jest emisja ze spalania w indywidualnych systemach grzewczych paliw stałych takich jak węgiel, drewno i biomasa oraz z ruchu drogowego, szczególnie z pojazdów z silnikami wysokoprężnymi bez filtrów cząstek stałych.\n\nPyły mogą stanowić poważny czynnik chorobotwórczy, osiadają na ściankach pęcherzyków płucnych utrudniając wymianę gazową, powodują podrażnienie naskórka i śluzówki, zapalenie górnych dróg oddechowych oraz wywołują choroby alergiczne, astmę, nowotwory płuc, gardła i krtani. Nie istnieje próg stężenia, poniżej którego negatywne skutki zdrowotne wynikające z oddziaływania pyłów na zdrowie ludzi nie występują. Grupą szczególnie narażoną na negatywne oddziaływanie pyłów są osoby starsze, dzieci i osoby cierpiące na choroby dróg oddechowych i układu krwionośnego."
    static var pm25 = "Pył zawieszony PM2,5 (particulate matter) jest mieszaniną substancji organicznych i nieorganicznych. Do atmosfery emitowany jest jako zanieczyszczenie pierwotne powstające w wyniku procesów antropogenicznych i naturalnych oraz jako zanieczyszczenie wtórne, powstające w wyniku przemian dwutlenku siarki, dwutlenku azotu, amoniaku, lotnych związków organicznych i trwałych związków organicznych.\n\nPył PM2,5 jest również zanieczyszczeniem transgranicznym, transportowanym na odległość do 2500 km. W powietrzu może pozostawać przez wiele dni lub tygodni, a sedymentacja i opady nie usuwają go z atmosfery.\n\nPył zawieszony PM2,5 przenika do najgłębszych partii płuc, gdzie jest akumulowany, stanowiąc poważny czynnik chorobotwórczy, osiada na ściankach pęcherzyków płucnych utrudniając wymianę gazową, powodują podrażnienie naskórka i śluzówki, zapalenie górnych dróg oddechowych oraz wywołując choroby alergiczne, astmę, nowotwory płuc, gardła i krtani."
    static var no2 = "Brunatny gaz o ostrym zapachu, silnie toksyczny dla układu oddechowego i odpornościowego.\n\nPowstaje m.in. w wyniku utlenienia tlenku azotu NO\n\nw wilgotnym powietrzu z NO2 tworzy się kwas azotowy HNO3 (składnik kwaśnego deszczu)."
    static var so2 = "Bezbarwny gaz o gryzącym i duszącym zapachu, silnie drażniący drogi oddechowe (nos, gardło, płuca).\n\nW wilgotnym powietrzu z SO2 tworzy się kwas siarkowy H2SO4 (składnik kwaśnego deszczu)."
    static var o3 = "Ozon to odmiana tlenu o cząsteczce trójatomowej. Jest to drażniący gaz o barwie bladoniebieskiej i charakterystycznej woni. Ozon obecny w warstwie atmosfery przy powierzchni ma negatywny wpływ na zdrowie ludzkie i roślinność. Jest jednym ze składników smogu fotochemicznego, powstającego głównie latem przy wysokich temperaturach i ciśnieniu w miastach o bardzo dużym ruchu samochodowym, ale najwyższe stężenia mogą występować na obszarach pozamiejskich dokąd transportowane są prekursory ozonu z miasta. Poziom docelowy stężenia 8-godzinnego dla ozonu wynosi 120 µg/m3 i może być przekraczany nie więcej niż 25 dni w ciągu roku. Poziom informowania społeczeństwa wynosi 180 µg/m3, a poziom alarmowy 240 µg/m3\n\nOzon w warstwie atmosfery przy powierzchni Ziemi to zanieczyszczenie wtórne - powstaje na skutek przemian fotochemicznych w powietrzu, powodowanych między innymi przez obecność prekursorów: tlenków azotu, węglowodorów i tlenku węgla. Ozon może powodować chwilowe zaburzenia funkcji oddechowych, szybki i płytki oddech oraz bóle głowy, zwłaszcza przy większym wysiłku fizycznym. Wysokie stężenia ozonu mogą powodować podrażnienia górnego odcinka dróg oddechowych, kaszel i napady duszności."
}

struct PollutionDescription {
    static func get(forId: Int) -> String {
        switch forId {
        case 1:
            return PollutionsDescriptionsStrings.pm10
        case 2:
            return PollutionsDescriptionsStrings.pm25
        case 3:
            return PollutionsDescriptionsStrings.no2
        case 6:
            return PollutionsDescriptionsStrings.so2
        case 8:
            return PollutionsDescriptionsStrings.o3
        default:
            return "Brak szczegółowego opisu zanieczyszczenia."
        }
    }
}
