import Foundation

struct StationsViewData {
    let name: String
    var pollutions = [PollutionsViewData]()
}
